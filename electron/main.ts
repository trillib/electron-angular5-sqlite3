import { app, BrowserWindow, ipcMain } from 'electron';
import * as path from 'path';
import * as url from 'url';
import * as knex from 'knex';
import { Person } from './person';

let mainWindow: Electron.BrowserWindow;
const personDB = knex({
  client: 'sqlite3',
  connection: {
    filename: path.join(__dirname, '../database/', 'person.sqlite')
}});

function createWindow() {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    height: 600,
    width: 800,
  });

  // and load the index.html of the app.
  mainWindow.loadURL(`file://${__dirname}/../dist/index.html`);
  logPerson();
  // Emitted when the window is closed.
  mainWindow.on('closed', () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null;
  });
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  // On OS X it"s common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow();
  }
});

ipcMain.on('receivePersons', () => {

});

function logPerson() {
  const result = personDB.select(['firstname', 'lastname']).from('Person');
  result.then(rows => {
    const persons = <Array<Person>> rows;
    for (const person of persons) {
      console.log(person.firstname);
    }
  });
}

// In this file you can include the rest of your app"s specific main process
// code. You can also put them in separate files and require them here.
